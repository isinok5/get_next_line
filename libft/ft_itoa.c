/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wsalmon <wsalmon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/18 14:41:48 by wsalmon           #+#    #+#             */
/*   Updated: 2019/09/22 21:28:24 by wsalmon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int		ft_get_size(unsigned int n)
{
	unsigned int size;

	size = 0;
	while (n >= 10)
	{
		n /= 10;
		size++;
	}
	size++;
	return (size);
}

char			*ft_itoa(int n)
{
	char			*str;
	unsigned int	nb;
	unsigned int	len;
	unsigned int	i;

	if (n < 0)
		nb = (unsigned int)(n * -1);
	else
		nb = (unsigned int)n;
	len = ft_get_size(nb);
	if (!(str = (char *)malloc(sizeof(char) * (len + 1 + (n < 0 ? 1 : 0)))))
		return (0);
	i = 0;
	if (n < 0 && len++)
		str[i] = '-';
	i = len - 1;
	while (nb >= 10)
	{
		str[i--] = (char)(nb % 10 + '0');
		nb /= 10;
	}
	str[i] = (char)(nb % 10 + '0');
	str[len] = '\0';
	return (str);
}
