/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wsalmon <wsalmon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/18 14:47:43 by wsalmon           #+#    #+#             */
/*   Updated: 2019/09/18 14:47:46 by wsalmon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_word_len(char const *s, char c)
{
	int i;
	int len;

	i = 0;
	len = 0;
	while (s[i] && s[i] != c)
	{
		len++;
		i++;
	}
	return (len);
}

char		**ft_strsplit(char const *s, char c)
{
	char	**arr;
	int		i;
	int		j;
	int		k;

	i = 0;
	j = 0;
	if (!s || !(arr = (char **)malloc(sizeof(char *) * (ft_c_w(s, c) + 1))))
		return (NULL);
	while (s[j])
	{
		k = 0;
		while (s[j] == c && s[j])
			j++;
		if (!(arr[i] = (char *)malloc(sizeof(char) *
				(ft_word_len(s + j, c) + 1))))
			return (NULL);
		while (s[j] != c && s[j])
			arr[i][k++] = s[j++];
		arr[i][k] = '\0';
		if (s[j] == c || s[j - 1] != c)
			i++;
	}
	arr[i] = 0;
	return (arr);
}
