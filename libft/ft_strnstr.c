/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wsalmon <wsalmon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/15 15:16:56 by wsalmon           #+#    #+#             */
/*   Updated: 2019/09/15 15:17:00 by wsalmon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *s, const char *find, size_t slen)
{
	size_t i;
	size_t pos;

	pos = 0;
	if (!*find)
		return ((char *)s);
	while (s[pos] && pos < slen)
	{
		if (s[pos] == find[0])
		{
			i = 1;
			while (find[i] && s[pos + i] &&
				s[pos + i] == find[i] &&
				(pos + i) < slen)
				i++;
			if (find[i] == '\0')
				return ((char *)&s[pos]);
		}
		pos++;
	}
	return (0);
}
