/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wsalmon <wsalmon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 15:36:52 by wsalmon           #+#    #+#             */
/*   Updated: 2019/09/19 15:36:56 by wsalmon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *l;
	t_list *list;
	t_list *node;

	if (!lst || !f)
		return (NULL);
	node = (*f)(lst);
	l = ft_lstnew(node->content, node->content_size);
	if (!l)
		return (NULL);
	list = l;
	lst = lst->next;
	while (lst)
	{
		node = (*f)(lst);
		l->next = ft_lstnew(node->content, node->content_size);
		if (!l)
			return (NULL);
		l = l->next;
		lst = lst->next;
	}
	return (list);
}
