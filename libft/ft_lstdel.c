/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wsalmon <wsalmon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/18 19:38:06 by wsalmon           #+#    #+#             */
/*   Updated: 2019/09/18 19:38:08 by wsalmon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*p;
	t_list	*buf;

	if (!alst || !del)
		return ;
	p = *alst;
	while (p)
	{
		buf = p;
		ft_lstdelone(&p, del);
		p = buf->next;
	}
	*alst = NULL;
}
