/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wsalmon <wsalmon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/13 20:03:24 by wsalmon           #+#    #+#             */
/*   Updated: 2019/09/13 20:03:26 by wsalmon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_memcmp(const void *buf1, const void *buf2, size_t count)
{
	const unsigned char *arr1;
	const unsigned char *arr2;

	if (buf1 == buf2 || count == 0)
		return (0);
	arr1 = (unsigned char *)buf1;
	arr2 = (unsigned char *)buf2;
	while (count)
	{
		if (*arr1 != *arr2)
			return (*arr1 - *arr2);
		arr1++;
		arr2++;
		count--;
	}
	return (0);
}
