/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wsalmon <wsalmon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/13 16:48:20 by wsalmon           #+#    #+#             */
/*   Updated: 2019/09/13 16:48:21 by wsalmon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *dest, int c, size_t n)
{
	unsigned char *arr;

	arr = (unsigned char *)dest;
	while (n > 0)
	{
		*arr = (unsigned char)c;
		arr++;
		n--;
	}
	return (dest);
}
