/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wsalmon <wsalmon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/18 19:12:19 by wsalmon           #+#    #+#             */
/*   Updated: 2019/09/18 19:12:21 by wsalmon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*str;
	char	*ptr;
	size_t	i;

	i = 0;
	if (!s)
		return (NULL);
	str = ft_strdup((char const *)s);
	if (!str)
		return (NULL);
	ptr = (char *)s;
	while (ptr[i])
	{
		str[i] = (*f)(ptr[i]);
		i++;
	}
	return (str);
}
