/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: admin <admin@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/16 17:09:17 by wsalmon           #+#    #+#             */
/*   Updated: 2019/10/27 13:01:54 by admin            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/libft.h"
#include "get_next_line.h"

int	get_next_line(const int fd, char **line)
{
	static char	*rem[2147483647];
	char		buf[BUFF_SIZE + 1];
	char		*tmp;
	ssize_t		bytes;
	int			end;

	if (fd < 0 || (!rem[fd] && !(rem[fd] = ft_strnew(1))) || !line)
		return (-1);
	while (!ft_strchr(rem[fd], '\n') && (bytes = read(fd, buf, BUFF_SIZE)) > 0)
	{
		buf[bytes] = '\0';
		tmp = rem[fd];
		rem[fd] = ft_strjoin(rem[fd], buf);
		ft_strdel(&tmp);
	}
	if (bytes == -1 || !*(tmp = rem[fd]))
		return (bytes == -1 ? -1 : 0);
	if ((end = (ft_strchr(rem[fd], '\n') > 0)))
	{
		*line = ft_strsub(rem[fd], 0, ft_strchr(rem[fd], '\n') - rem[fd]);
	}
	else
		*line = ft_strdup(rem[fd]);
	rem[fd] = ft_strsub(rem[fd], (unsigned int)(ft_strlen(*line) + end),
		(size_t)(ft_strlen(rem[fd]) - (ft_strlen(*line) + end)));
	ft_strdel(&tmp);
	return (!(!rem[fd] && !ft_strlen(*line)));
}
